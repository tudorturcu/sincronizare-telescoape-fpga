----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:23:44 10/20/2014 
-- Design Name: 
-- Module Name:    counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counterSeconds is
	Generic	( 	
					inputSize: natural := 6;
					outputSize: natural := 6
				);
	Port	(
				clk: in std_logic;
				set: in std_logic;
				input: in std_logic_vector (inputSize * 4 - 1 downto 0);
				enable: in std_logic;
				output: out std_logic_vector (outputSize * 4 - 1 downto 0)
			);
end counterSeconds;

architecture Behavioral of counterSeconds is

Signal count: std_logic_vector (outputSize * 4 - 1 downto 0);

begin

process (clk, set)
begin
	
		if (set = '1') then
			count (outputSize * 4 - 1  downto (outputSize - inputSize) * 4) <= input;
			count ((outputSize - inputSize) * 4 - 1 downto 0) <= std_logic_vector (to_unsigned(0, (outputSize - inputSize) * 4));
	elsif enable = '1' then --!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!maybe not good
		if (rising_edge(clk)) then -- add smaller units ONLY if you also change the clock!!!
			
			if (count((outputSize - 5) * 4 - 1 downto (outputSize - 6) * 4) = x"9") then	-- smallSecond
				count((outputSize - 5) * 4 - 1 downto (outputSize - 6) * 4) <= x"0";
				if (count((outputSize - 4) * 4 - 1 downto (outputSize - 5) * 4) = x"5") then -- bigSecond
					count((outputSize - 4) * 4 - 1 downto (outputSize - 5) * 4) <= x"0";
					if (count((outputSize - 3) * 4 - 1 downto (outputSize - 4) * 4) = x"9") then -- smallMinute
						count((outputSize - 3) * 4 - 1 downto (outputSize - 4) * 4) <= x"0";
						if (count((outputSize - 2) * 4 - 1 downto (outputSize - 3) * 4) = x"5") then -- bigMinute
							count((outputSize - 2) * 4 - 1 downto (outputSize - 3) * 4) <= x"0";
							if (count((outputSize - 1) * 4 - 1 downto (outputSize - 2) * 4) = x"9") then -- smallHour - case 1 (09 si 19)
								count((outputSize - 1) * 4 - 1 downto (outputSize - 2) * 4) <= x"0";
								count((outputSize - 0) * 4 - 1 downto (outputSize - 1) * 4) <= std_logic_vector (unsigned(count((outputSize - 0) * 4 - 1 downto (outputSize - 1) * 4) ) + 1);
							elsif (count((outputSize - 0) * 4 - 1 downto (outputSize - 2) * 4) = x"23") then -- end of day
								count((outputSize - 0) * 4 - 1 downto (outputSize - 2) * 4) <= x"00";
							else -- regular hour increment
								count((outputSize - 1) * 4 - 1 downto (outputSize - 2) * 4) <= std_logic_vector (unsigned(count((outputSize - 1) * 4 - 1 downto (outputSize - 2) * 4) ) + 1);
							end if;
						else -- regular bigMinute increment
							count((outputSize - 2) * 4 - 1 downto (outputSize - 3) * 4) <= std_logic_vector (unsigned(count((outputSize - 2) * 4 - 1 downto (outputSize - 3) * 4) ) + 1);
						end if;
					else -- regular smallMinute increment
						count((outputSize - 3) * 4 - 1 downto (outputSize - 4) * 4) <= std_logic_vector (unsigned(count((outputSize - 3) * 4 - 1 downto (outputSize - 4) * 4) ) + 1);
					end if;
				else -- regular bigSecond increment
					count((outputSize - 4) * 4 - 1 downto (outputSize - 5) * 4) <= std_logic_vector (unsigned(count((outputSize - 4) * 4 - 1 downto (outputSize - 5) * 4) ) + 1);
				end if;
			else -- regular smallSecond increment
				count((outputSize - 5) * 4 - 1 downto (outputSize - 6) * 4) <= std_logic_vector (unsigned(count((outputSize - 5) * 4 - 1 downto (outputSize - 6) * 4) ) + 1);
			end if;
					
		end if;	-- rising_edge(clk)
	end if;
end process;

output <= count;

end Behavioral;

