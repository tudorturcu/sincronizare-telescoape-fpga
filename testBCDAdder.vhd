----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:22:32 03/18/2015 
-- Design Name: 
-- Module Name:    testBCDAdder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity testBCDAdder is
	port (
				sw		: in std_logic_vector (8 downto 0);
				led	: out std_logic_vector (4 downto 0)
			);
end testBCDAdder;

architecture Behavioral of testBCDAdder is

component BCDBaseSixAdder is
	Port	(
				a			: in std_logic_vector (3 downto 0);
				b			: in std_logic_vector (3 downto 0);
				carryIn	: in std_logic;
				sum		: out std_logic_vector (3 downto 0);
				carryOut	: out std_logic
			);

end component;


begin

	adderTest: BCDBaseSixAdder
	port map (
		a => sw (7 downto 4),
		b => sw (3 downto 0),
		carryIn => sw (8),
		sum => led (3 downto 0),
		carryOut => led (4)
	);
	

end Behavioral;

