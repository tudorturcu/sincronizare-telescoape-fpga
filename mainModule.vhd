----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:57:18 10/21/2014 
-- Design Name: 
-- Module Name:    mainModule - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mainModule is

	Port (
				clk				: in std_logic;
				led				: out std_logic_vector (15 downto 0);
				btn				: in std_logic_vector (4 downto 0);
				sw					: in std_logic_vector (15 downto 0);
				an					: out std_logic_vector (7 downto 0);
				cat				: out std_logic_vector (6 downto 0);
				gpsRx				: in std_logic;
				gpsTx				: out std_logic;
				rx					: in std_logic;
				tx					: out std_logic;
				trigger			: out std_logic;
				pulsePerSecond	: in std_logic;
				dp					: out std_logic
			);
end mainModule;

architecture Behavioral of mainModule is

---------------------------  CONSTANTS   ---------------------------
constant BAUD_RATE							: positive := 9600;
constant	clockConstant						: positive := 100000000;
constant CLOCK_FREQUENCY					: positive := clockConstant;
constant timeThresholdForSwitchingModes: positive := 4;
constant	outputSizeCounter					: natural := 9;
constant	inputSizeCounter					: natural := 9;
constant syncTimeSize						: natural := 6;
constant gpsTimeSize							: natural := 6;

constant	gpsRamAddressSize					: natural := 7;
constant gpsRamDataSize						: natural := 8;

constant exposuresRamAddressSize			: natural := 10;
constant exposuresRamDataSize				: natural := syncTimeSize * 4;
constant EXPOSURES_END						: std_logic_vector (syncTimeSize * 4 - 1 downto 0) := x"999999";
constant defaultCounterResetValue		: std_logic_vector (syncTimeSize * 4 - 1 downto 0) := x"235930";

constant pulseNumber							: natural := 5;

---------------------------  COMPONENTS  ---------------------------
component SSD_8digits is
		Port ( clk 			: in  STD_LOGIC;
				  d1 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d2 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d3 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d4 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d5 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d6 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d7 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d8 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  dpVector 	: in 	STD_LOGIC_VECTOR (7 downto 0);
				  
				  an 			: out STD_LOGIC_VECTOR (7 downto 0);
				  cat 		: out STD_LOGIC_VECTOR (6 downto 0);
				  dp 			: out STD_LOGIC);
end component;

component SDD is
    Port ( clk : in  STD_LOGIC;
           d1 : in  STD_LOGIC_VECTOR (3 downto 0);
           d2 : in  STD_LOGIC_VECTOR (3 downto 0);
           d3 : in  STD_LOGIC_VECTOR (3 downto 0);
           d4 : in  STD_LOGIC_VECTOR (3 downto 0);
           an : out  STD_LOGIC_VECTOR (3 downto 0);
           cat : out  STD_LOGIC_VECTOR (6 downto 0);
			  dp : out STD_LOGIC);
end component;

component mono_pulse_generator is
Generic 	(
				pulseNumber: natural
			);

Port (
			clk: in std_logic;
			step: out std_logic_vector (pulseNumber - 1 downto 0);
			btn: in std_logic_vector (pulseNumber - 1 downto 0)
		);	
			
end component;

component decimalCounter is
	Port 	(
				
				d: in std_logic_vector (3 downto 0);
				set: in std_logic;
				clk: in std_logic;
				
				q: out std_logic_vector (3 downto 0);
				overflow: out std_logic
			);
				
end component;

component oneSecondClock is
	Generic 	(
					clockConstant		: natural := 50000000;
					maximumVectorSize	: natural := 32
				);
		Port 	(
					clockIn				: in std_logic;
					clockOut				: out std_logic
				);
end component;

component counter is
	Generic	( 	
					inputSize		: natural := 9;
					outputSize		: natural := 9
				);
		Port	(
					milliClk			: in std_logic;
					set				: in std_logic;
					input				: in std_logic_vector (inputSize * 4 - 1 downto 0);
					output			: out std_logic_vector (outputSize * 4 - 1 downto 0)
				);
end component;

component ramMemory is
	generic
	(
		ramAddressSize	: natural;	-- |log_2(ramSize)|
		ramDataSize		: natural
	);
	port 
	(
		readAddress				: in std_logic_vector (ramAddressSize - 1 downto 0);
		readAddressSecondary	: in std_logic_vector (ramAddressSize - 1 downto 0);
		writeAddress			: in std_logic_vector (ramAddressSize - 1 downto 0);
		clk						: in std_logic;
		chipSelect				: in std_logic;
		writeEnable				: in std_logic;
		dataIn					: in std_logic_vector (ramDataSize - 1 downto 0);
		dataOut					: out std_logic_vector (ramDataSize - 1 downto 0);
		dataOutSecondary		: out std_logic_vector (ramDataSize - 1 downto 0)
	);
	
end component;

component UART is
        generic (
                BAUD_RATE           : positive;
                CLOCK_FREQUENCY     : positive
            );
        port (  -- General
                CLOCK               :   in      std_logic;
                RESET               :   in      std_logic;    
                DATA_STREAM_IN      :   in      std_logic_vector(7 downto 0);
                DATA_STREAM_IN_STB  :   in      std_logic;
                DATA_STREAM_IN_ACK  :   out     std_logic;
                DATA_STREAM_OUT     :   out     std_logic_vector(7 downto 0);
                DATA_STREAM_OUT_STB :   out     std_logic;
                DATA_STREAM_OUT_ACK :   in      std_logic;
                TX                  :   out     std_logic;
                RX                  :   in      std_logic
             );
    end component UART;
	 
component counterSeconds is
	Generic	( 	
					inputSize: natural := 6;
					outputSize: natural := 6
				);
	Port	(
				clk: in std_logic;
				set: in std_logic;
				input: in std_logic_vector (inputSize * 4 - 1 downto 0);
				enable: in std_logic;
				output: out std_logic_vector (outputSize * 4 - 1 downto 0)
			);
end component;	 

---------------------------  FUNCTIONS   ---------------------------

-- input conversion ---------------------------------------

function vectorize(s: std_logic) return std_logic_vector is
variable v: std_logic_vector(0 downto 0);
begin
v(0) := s;
return v;
end;

function vectorize(v: std_logic_vector) return std_logic_vector is
begin
return v;
end;


---------------------------   SIGNALS    ---------------------------
type	gpsTimeLoadStates is 		(	idle,
												firstCheck,
												secondCheck,
												readTime,
												invalid,
												valid
											);

signal loadGpsTimeState				: gpsTimeLoadStates := idle;
signal stateLeds						: std_logic_vector (2 downto 0);

Signal ssd_in							: std_logic_vector (31 downto 0);
Signal ssd_dp_in						: std_logic_vector (7 downto 0);
Signal outputDecimalCounter		: std_logic_vector (3 downto 0);
Signal step								: std_logic_vector (pulseNumber - 1 downto 0);
Signal clk1second						: std_logic;

Signal clkMicro						: std_logic;
Signal clkMilli						: std_logic;
Signal outputCounter					: std_logic_vector (4 * outputSizeCounter - 1 downto 0);
Signal outputFutureCounter			: std_logic_vector (4 * outputSizeCounter - 1 downto 0);
signal counterResetValue			: std_logic_vector (4 * inputSizeCounter - 1 downto 0);
signal futureCounterResetValue	: std_logic_vector (4 * inputSizeCounter - 1 downto 0)		:= x"235905000";
signal counterSet						: std_logic;
signal futureCounterSet				: std_logic;
signal resetCounters					: std_logic;

signal timeSinceLastPPS				: std_logic_vector (3 downto 0);
signal resetLoadGpsTime				: std_logic;
signal gpsTime							: std_logic_vector (4 * gpsTimeSize - 1 downto 0);
signal gpsDataValid					: std_logic;

Signal gpsRamReadAddress			: std_logic_vector (gpsRamAddressSize - 1 downto 0);
Signal gpsRamReadAddressSecondary: std_logic_vector (gpsRamAddressSize - 1 downto 0);
Signal gpsRamWriteAddress			: std_logic_vector (gpsRamAddressSize - 1 downto 0);
Signal gpsRamChipSelect				: std_logic;
Signal gpsRamWriteEnable			: std_logic;
Signal gpsRamDataWrite				: std_logic_vector (7 downto 0);
Signal gpsRamDataRead				: std_logic_vector (7 downto 0);
Signal gpsRamDataReadSecondary	: std_logic_vector (7 downto 0);
Signal ramWriteCountClk				: std_logic;
Signal ramReadCountClk				: std_logic;
signal resetRamDisplay				: std_logic;
signal charactersRead				: natural range 0 to 5;


signal quarterSecondSignal			: std_logic;

signal gpsTransmitByte				: std_logic_vector(7 downto 0);
signal gpsReceiveByte				: std_logic_vector(7 downto 0);
signal gpsTransmitStrobe			: std_logic;
signal gpsTransmitAcknowledge		: std_logic;
signal gpsReceiveStrobe				: std_logic;
signal gpsReceiveAcknowledge		: std_logic;
signal gpsReset						: std_logic;

signal pulsePerSecondSignal		: std_logic;

signal exposuresStartingTime		: std_logic_vector (4 * syncTimeSize - 1 downto 0);
signal intervalBetweenExposures	: std_logic_vector (5 downto 0);	-- TODO: de modificat!!!
signal exposureTimeInSeconds		: std_logic_vector (3 downto 0); -- TODO: de modificat!!!
signal numberOfExposures			: std_logic_vector (9 downto 0);

signal nextExposureStart			: std_logic_vector (4 * syncTimeSize - 1 downto 0);
signal nextExposureEnd				: std_logic_vector (4 * syncTimeSize - 1 downto 0);
signal numberOfExposuresSoFar		: std_logic_vector (15 downto 0);
signal lastExposureStartPerformed: std_logic_vector (4 * syncTimeSize - 1 downto 0);
signal lastExposureEndPerformed	: std_logic_vector (4 * syncTimeSize - 1 downto 0);
signal outputFutureCounterTrimmed: std_logic_vector (4 * syncTimeSize - 1 downto 0);
signal resetNextExposure			: std_logic;

signal exposureEndRamReadAddress		: std_logic_vector (exposuresRamAddressSize - 1 downto 0);
signal exposureEndRamWriteAddress	: std_logic_vector (exposuresRamAddressSize - 1 downto 0);
signal exposureEndRamChipSelect		: std_logic;
signal exposureEndRamWriteEnable		: std_logic;
signal exposureEndRamDataWrite		: std_logic_vector (exposuresRamDataSize - 1 downto 0);
signal exposureEndRamDataRead			: std_logic_vector (exposuresRamDataSize - 1 downto 0);

signal exposureStartRamReadAddress				: std_logic_vector (exposuresRamAddressSize - 1 downto 0);
signal exposureStartRamReadAddressSecondary	: std_logic_vector (exposuresRamAddressSize - 1 downto 0);
signal exposureStartRamWriteAddress				: std_logic_vector (exposuresRamAddressSize - 1 downto 0);
signal exposureStartRamChipSelect				: std_logic;
signal exposureStartRamWriteEnable				: std_logic;
signal exposureStartRamDataWrite					: std_logic_vector (exposuresRamDataSize - 1 downto 0);
signal exposureStartRamDataRead					: std_logic_vector (exposuresRamDataSize - 1 downto 0);
signal exposureStartRamDataReadSecondary		: std_logic_vector (exposuresRamDataSize - 1 downto 0);

signal exposuresCalculatorSet			: std_logic;
signal exposuresCalculatorResetValue: std_logic_vector (4 * syncTimeSize - 1 downto 0);
signal exposuresCalculatorCounter	: std_logic_vector (4 * syncTimeSize - 1 downto 0);
signal enableCounter						: std_logic;

signal resetExposureGenerator			: std_logic;


type EXPOSURESTATES is ( 			idle,
											checkNumber,
											countSeconds,
											stop);
signal generateExposuresState			: EXPOSURESTATES := idle;
signal generateExposuresNextState	: EXPOSURESTATES := idle;

signal startGenerator					: std_logic;
signal exposureGeneratorStopSignal	: std_logic;
signal testGenerator						: std_logic;
signal exposuresSecondCount			: integer;
signal exposuresGenerated				: integer;

type exposures is array (0 to 2**exposuresRamAddressSize - 1) of std_logic_vector (exposuresRamDataSize - 1 downto 0);
signal exposureStarts: exposures :=		
	(
		x"235845",
		x"235850",
		x"235855",
		x"235900",
		x"235905",
		x"235910",
		x"235915",
		x"235920",
		x"235925",
		x"235930",
		x"235935",
		x"235940",
		x"235945",
		x"235950",
		x"235955",
		x"000000",
		x"000003",
		x"000006",
		x"000009",
		x"000012",
		x"000015",
		x"000018",
		x"000021",
		x"000024",
		x"000027",
		x"000030",
		x"000033",
		x"000036",
		x"000039",
		x"000042",
		x"000045",
		x"000048",
		x"000051",
		x"000054",
		x"000057",
		x"999999",
		others => (others => '0')
	);
	
signal exposureEnds: exposures :=		
	(
x"235846",
		x"235851",
		x"235856",
		x"235901",
		x"235906",
		x"235911",
		x"235916",
		x"235921",
		x"235926",
		x"235931",
		x"235936",
		x"235941",
		x"235946",
		x"235951",
		x"235956",
		x"000001",
		x"000004",
		x"000007",
		x"000010",
		x"000013",
		x"000016",
		x"000019",
		x"000022",
		x"000025",
		x"000028",
		x"000031",
		x"000034",
		x"000037",
		x"000040",
		x"000043",
		x"000046",
		x"000049",
		x"000052",
		x"000055",
		x"000058",
		x"999999",
		others => (others => '0')
	);




type syncModes is ( 					ppsAndValid,
											ppsAndInvalid,
											noPPSAndValid,
											noPPSAndInvalid );
signal syncMode						: syncModes := ppsAndValid;


signal selectedClock					: std_logic;


signal ssdSwitchCounter				: std_logic_vector (3 downto 0);
signal switchSsdSignal				: std_logic;

signal dummySignal					: std_logic;

signal masterReset					: std_logic;
signal resetTriggerProcess			: std_logic;

signal triggerSignal					: std_logic;

signal setIntervals					: std_logic;

signal exposureStateLeds			: std_logic_vector (2 downto 0);

begin

		--port components
		SSDisplay: SSD_8digits 
		Port map
		( clk 		=> clk,
		  d1 			=> ssd_in (3 downto 0),
		  d2 			=> ssd_in (7 downto 4),
		  d3 			=> ssd_in (11 downto 8),
		  d4 			=> ssd_in (15 downto 12),
		  d5 			=> ssd_in (19 downto 16),
		  d6 			=> ssd_in (23 downto 20),
		  d7 			=> ssd_in (27 downto 24),
		  d8 			=> ssd_in (31 downto 28),
		  dpVector 	=> ssd_dp_in,
		  
		  an 			=> an,
		  cat 		=> cat,
		  dp 			=> dp
		);
				
		monoPulseGenerator: mono_pulse_generator 
		generic map
		(
			pulseNumber => pulseNumber
		)
		port map
		(
			clk => clk,
			step => step,
			btn => btn
		);	
		
		pulsePerSecondGenerator: mono_pulse_generator
		generic map
		(
			pulseNumber => 1
		)
		port map
		(
			clk => clk,
			step(0) => pulsePerSecondSignal,
			btn(0) => pulsePerSecond
		);
			
		clockDivider1second: oneSecondClock 
		generic map 
		(
			clockConstant => clockConstant
		)
		port map 	
		(
			clockIn => clk,
			clockOut => clk1second
		);		
		
		clockMillisecond: oneSecondClock 
		generic map 
		(
			clockConstant => clockConstant / 1000
		)
		port map 	
		(
			clockIn => clk,
			clockOut => clkMilli
		);	
		
		clockMicrosecond: oneSecondClock
		generic map
		(
			clockConstant => clockConstant / 1000000
		)
		port map
		(
			clockIn => clk,
			clockOut => clkMicro
		);
		
		exposuresCalculatorClock: counterSeconds
		generic map
		(
			outputSize => syncTimeSize
		)
		Port map
		(
			clk => clk,
			set => exposuresCalculatorSet,
			input => exposuresCalculatorResetValue,
			enable => enableCounter,
			output => exposuresCalculatorCounter
		);
		
		
		standardClockCounter: counter
		generic map
		(
			outputSize => outputSizeCounter
		)
		Port map	
		(
			milliClk => clkMilli,
			set => counterSet,
			input => counterResetValue,
			output => outputCounter
		);
		
		futureClockCounter: counter
		generic map
		(
			outputSize => outputSizeCounter
		)
		Port map	
		(
			milliClk => clkMilli,
			set => futureCounterSet,
			input => futureCounterResetValue,
			output => outputFutureCounter
		);
		
		storeGPSData: ramMemory
		generic map
		(
			ramAddressSize => gpsRamAddressSize,
			ramDataSize		=> gpsRamDataSize
		)
		port map
		(
			readAddress => gpsRamReadAddress,
			readAddressSecondary => gpsRamReadAddressSecondary,
			writeAddress => gpsRamWriteAddress,
			clk => clk,
			chipSelect => gpsRamChipSelect,
			writeEnable => gpsRamWriteEnable,
			dataIn => gpsRamDataWrite,
			dataOut => gpsRamDataRead,
			dataOutSecondary => gpsRamDataReadSecondary
		);
		
		gpsUARTProcessing : UART
		 generic map (
					BAUD_RATE           => BAUD_RATE,
					CLOCK_FREQUENCY     => CLOCK_FREQUENCY
		 )
		 port map    (  
					-- General
					CLOCK               => clk,
					RESET               => gpsReset,
					DATA_STREAM_IN      => gpsTransmitByte,
					DATA_STREAM_IN_STB  => gpsTransmitStrobe,
					DATA_STREAM_IN_ACK  => gpsTransmitAcknowledge,
					DATA_STREAM_OUT     => gpsReceiveByte,
					DATA_STREAM_OUT_STB => gpsReceiveStrobe,
					DATA_STREAM_OUT_ACK => gpsReceiveAcknowledge,
					--TX                  => gpsTx, -- nu folosim partea de transmisie, facem o simpla legatura intre calculator si gps cu un fir pe tx
					RX                  => gpsRx
		 );
		 
		storeExposureStart: ramMemory
		generic map
		(
			ramAddressSize => exposuresRamAddressSize,
			ramDataSize		=> exposuresRamDataSize
		)
		port map
		(
			readAddress => exposureStartRamReadAddress,
			readAddressSecondary => exposureStartRamReadAddressSecondary,
			writeAddress => exposureStartRamWriteAddress,
			clk => clk,
			chipSelect => exposureStartRamChipSelect,
			writeEnable => exposureStartRamWriteEnable,
			dataIn => exposureStartRamDataWrite,
			dataOut => exposureStartRamDataRead,
			dataOutSecondary => exposureStartRamDataReadSecondary
		);
		
		storeExposureEnd: ramMemory
		generic map
		(
			ramAddressSize => exposuresRamAddressSize,
			ramDataSize		=> exposuresRamDataSize
		)
		port map
		(
			readAddress => exposureEndRamReadAddress,
			readAddressSecondary => "0000000000",
			writeAddress => exposureEndRamWriteAddress,
			clk => clk,
			chipSelect => exposureEndRamChipSelect,
			writeEnable => exposureEndRamWriteEnable,
			dataIn => exposureEndRamDataWrite,
			dataOut => exposureEndRamDataRead,
			dataOutSecondary => open
		);

		
		------------------------------------ PROCESSES -----------------------------------
		
		-- proces stocat date GPS in memorie
		storeGpsDataToMemory: process (clk)
		begin
			if rising_edge(clk) then
				if gpsReset = '1' then
					gpsTransmitStrobe 		<= '0';
					gpsReceiveAcknowledge	<= '0';
					gpsTransmitByte			<= (others => '0');
					gpsRamWriteEnable 		<= '0';
					gpsRamChipSelect 			<= '0';
				else
					-- Enable memory
					gpsRamChipSelect <= '1';
					gpsRamWriteEnable <= '1';
					-- Acknowledge data receive strobes 
					gpsReceiveAcknowledge		<= '0';
					if gpsReceiveStrobe = '1' then
					  gpsReceiveAcknowledge		<= '1';
					  -- store in memory
					  gpsRamWriteEnable <= '1';
					  gpsRamDataWrite <= gpsReceiveByte;
					  -- optional for testing !!!!!!!!!!!!!!!!!!!!!!!!
					  -- set up a transmission request (loopback)
					  gpsTransmitStrobe	<= '1';
					  gpsTransmitByte		<= gpsReceiveByte;	
					end if;
					-- Clear transmission request strobe upon acknowledge.
					if gpsTransmitAcknowledge = '1' then
					  gpsTransmitStrobe	<= '0';
					end if;	
				end if;
			end if;
		end process;
		
		-- proces resetat memoria cand dam de '$' si de incrementare a adresei de scriere din memoria RAM cand primim un nou caracter de la GPS
		process (ramWriteCountClk, gpsRamDataWrite)
		begin
			if (rising_edge(ramWriteCountClk)) then
				if (gpsRamDataWrite = "00100100") then				-- 24x  -> dollar sign in ascii marking the beginning of a new message
					gpsRamWriteAddress <= "0000000";
				else 
					gpsRamWriteAddress <= std_logic_vector (unsigned (gpsRamWriteAddress) + 1);
				end if;
			end if;
		end process;
		
		-- proces de incrementare a adresei de citire din memoria RAM
		process (ramReadCountClk)
		begin
			if resetRamDisplay = '1' then
				gpsRamReadAddress <= (others => '0');
				exposureStartRamReadAddressSecondary <= (others => '0');
			elsif (rising_edge(ramReadCountClk)) then
				gpsRamReadAddress <= std_logic_vector (unsigned (gpsRamReadAddress) + 1); 
				exposureStartRamReadAddressSecondary <= std_logic_vector (unsigned (exposureStartRamReadAddressSecondary) + 1);
			end if;
		end process;
		
		
		-- proces counter pt schimbat continut SSD
		process (switchSsdSignal, clk)
		begin
			if (rising_edge(clk)) then
				if (switchSsdSignal = '1') then
					ssdSwitchCounter <= std_logic_vector (unsigned (ssdSwitchCounter) + 1);
				end if;
			end if;
		end process;
		
		-- proces schimbat continut SSD
		process (clk)
		begin
			if (rising_edge(clk)) then
				case ssdSwitchCounter is
					when "0000" =>
						ssd_in (31 downto 0)	<= outputCounter (outputSizeCounter * 4 - 1 downto (outputSizeCounter - 8) * 4);
						ssd_dp_in				<= "10101011";
--					when "0001" =>				-- write address & data from RAM
--						ssd_in (7 downto 0) 	<= gpsRamDataWrite;
--						ssd_in (15 downto 8) <= '0' & gpsRamWriteAddress;	
					
					
					
--					when "0010" =>				-- exposure starts - last performed (ss) & next (ss)
--						ssd_in (7 downto 0) 	<= nextExposureStart (7 downto 0);
--						ssd_in (15 downto 8) <= lastExposureStartPerformed (7 downto 0);
--					when "0011" =>				-- exposure ends - last performed (ss) & next (ss)
--						ssd_in (7 downto 0) 	<= nextExposureEnd (7 downto 0);
--						ssd_in (15 downto 8) <= lastExposureEndPerformed (7 downto 0);	
--					when "0100" =>				-- outputCounter (ss) & outputFutureCounter (ss)
--						ssd_in (7 downto 0)	<= outputCounter (19 downto 12);
--						ssd_in (15 downto 8) <= outputFutureCounter (19 downto 12);
--					when "0101" =>				-- timeSinceLastPPS
--						ssd_in (3 downto 0)	<= timeSinceLastPPS; 
--						ssd_in (15 downto 4) <= x"000";
--					when "0110" =>
--						ssd_in (15 downto 0)	<= futureCounterResetValue (27 downto 12);
--					
--					when "0111" => 			-- read address & data from RAM
--						ssd_in (7 downto 0) 	<= gpsRamDataRead;
--						ssd_in (15 downto 8) <= '0' & gpsRamReadAddress;	
--					when "0001" =>
--						ssd_in (31 downto 0) <= x"00" & gpsTime;
					when "0001" => 	-- numberOfExposures & intervalBetweenExposures
						ssd_in (31 downto 0) <= x"A" & "00" & numberOfExposures & "0000000000" & intervalBetweenExposures;
						ssd_dp_in				<= "01101111";
					when "0010" =>
						ssd_in (31 downto 0) <= x"B0" & exposuresStartingTime;
						ssd_dp_in				<= "01101011";
--					when "0100" =>
--						ssd_in (31 downto 0) <= exposureStartRamReadAddress (7 downto 0) & exposureStartRamDataRead;
					when "0011" =>
						ssd_in (31 downto 0) <= exposureStartRamReadAddressSecondary (7 downto 0) & exposureStartRamDataReadSecondary;
						ssd_dp_in				<= "10101011";
					when "0100" =>
						ssd_in (31 downto 0) <= x"C0" & nextExposureStart;
						ssd_dp_in				<= "01101011";
					when "0101" =>
						ssd_in (31 downto 0) <= x"D0" & lastExposureStartPerformed;
						ssd_dp_in				<= "01101011";
					when "0110" =>
						ssd_in (31 downto 0) <= x"E000000" & timeSinceLastPPS; 
						ssd_dp_in				<= "01111101";
					-- debugging
					
--					when "0110" =>
--						ssd_in (31 downto 0) <= x"00" & exposuresCalculatorCounter;
--					when "0111" =>
--						ssd_in (31 downto 0) <= x"00" & nextExposureStart;
--					when "1000" =>
--						ssd_in (31 downto 0) <= x"00" & lastExposureStartPerformed;

--					when "1010" =>
--						ssd_in (31 downto 0) <= x"00" & lastExposureEndPerformed;
--					when "1011" =>
--						ssd_in (31 downto 0) <= exposureEndRamReadAddress (7 downto 0) & exposureEndRamDataRead;
--					when "1100" =>
--						ssd_in (31 downto 0) <= x"00" & outputFutureCounterTrimmed;
--					when "1101" =>
--						ssd_in (31 downto 0) <= outputFutureCounterTrimmed (15 downto 0) & nextExposureStart (15 downto 0);
--					when "1110" =>
--						ssd_in (31 downto 0) <= outputCounter (27 downto 12) & outputFutureCounter (27 downto 12);


					when others =>
						ssd_in <= (others => '1');
				end case;
			
			end if;
		end process;
		
		-- cronometrul care imi zice de cate secunde nu am mai primit pps		
		process (clk1second, pulsePerSecondSignal)		
		begin
			if pulsePerSecondSignal = '1' then		-- reset cronometru
				timeSinceLastPPS <= (others => '0');
			elsif rising_edge(clk1second) then
				if (timeSinceLastPPS = "1111") then
					timeSinceLastPPS <= "1111";
				else	
					timeSinceLastPPS <= std_logic_vector (unsigned (timeSinceLastPPS) + 1);
				end if;

--				if unsigned(timeSinceLastPPS) < timeThresholdForSwitchingModes then
--				
--				end if;
			end if;
		end process;		
		
		-- proces care imi zice urmatorul timp pentru urmatoarea expunere, cu masura de siguranta pentru 
		-- 
		
		
		process (clk)--(lastExposureStartPerformed, exposureStartRamDataRead, lastExposureEndPerformed, exposureEndRamDataRead, outputFutureCounterTrimmed)
		begin
			if resetNextExposure = '1' then
				exposureStartRamReadAddress <= (others => '0');
			elsif rising_edge (clk) then
				if lastExposureStartPerformed /= EXPOSURES_END then
				--OR lastExposureEndPerformed /= EXPOSURES_END then
					
					-- START
					if lastExposureStartPerformed <= exposureStartRamDataRead then
						if outputFutureCounterTrimmed >= lastExposureStartPerformed AND outputFutureCounterTrimmed <= exposureStartRamDataRead then
							-- found
							nextExposureStart <= exposureStartRamDataRead;
							exposureStateLeds <= "000";
						else
							-- keep looking
							if (unsigned(exposureStartRamReadAddress) < unsigned(numberOfExposures) - 1) then 	-- nu suntem inca la ultima expunere
								exposureStartRamReadAddress <= std_logic_vector (unsigned (exposureStartRamReadAddress) + 1);
							else																				-- suntem la ultima expunere (exposureStartRamReadAddress = numberOfExposures - 1)
								exposureStartRamReadAddress <= (others => '0');
							end if;
							
							exposureStateLeds <= "001";
						end if;
					else -- lastExposureStartPerformed > exposureStartRamDataRead
						if (outputFutureCounterTrimmed >= lastExposureStartPerformed AND outputFutureCounterTrimmed <= x"235959") OR
							(outputFutureCounterTrimmed >= x"000000" AND outputFutureCounterTrimmed <= exposureStartRamDataRead) then
							-- found
							nextExposureStart <= exposureStartRamDataRead;
							exposureStateLeds <= "010";
						else
							-- keep looking
							if (unsigned(exposureStartRamReadAddress) < unsigned(numberOfExposures) - 1) then 	-- nu suntem inca la ultima expunere
								exposureStartRamReadAddress <= std_logic_vector (unsigned (exposureStartRamReadAddress) + 1);
							else																				-- suntem la ultima expunere (exposureStartRamReadAddress = numberOfExposures - 1)
								exposureStartRamReadAddress <= (others => '0');
							end if;
							
							exposureStateLeds <= "011";
						end if;
					end if;
					
					--END
					if lastExposureEndPerformed <= exposureEndRamDataRead then
						if outputFutureCounterTrimmed >= lastExposureEndPerformed AND outputFutureCounterTrimmed <= exposureEndRamDataRead then
							-- found
							nextExposureEnd <= exposureEndRamDataRead;
						else
							-- keep looking
							exposureEndRamReadAddress <= std_logic_vector (unsigned (exposureEndRamReadAddress) + 1);
						end if;
					else -- lastExposureEndPerformed > exposureEndRamDataRead
						if (outputFutureCounterTrimmed >= lastExposureEndPerformed AND outputFutureCounterTrimmed <= x"235959") OR
							(outputFutureCounterTrimmed >= x"000000" AND outputFutureCounterTrimmed <= exposureEndRamDataRead) then
							-- found
							nextExposureEnd <= exposureEndRamDataRead;
						else
							-- keep looking
							exposureEndRamReadAddress <= std_logic_vector (unsigned (exposureEndRamReadAddress) + 1);
						end if;
					end if;
					
				end if;
			end if;
		end process;

		-- generate exposure times from parameters
		generateExposureTimes_combinatorial: process (clk, generateExposuresState, startGenerator, exposuresGenerated, exposuresSecondCount, testGenerator)
		begin			
			if rising_edge(clk) then
				case generateExposuresState is
					
					when idle =>
						enableCounter <= '0';
						if startGenerator = '1' then
							stateLeds <= "000";
							exposuresCalculatorResetValue <= exposuresStartingTime;
							exposuresCalculatorSet <= '1';
							
							exposuresGenerated <= 0;
							exposureStartRamWriteAddress <= "0000000000";
							exposureStartRamWriteEnable <= '1';
							
							generateExposuresNextState <= checkNumber;
						else
							generateExposuresNextState <= idle;
						end if;
						
					when checkNumber => 
						enableCounter <= '0';
						if exposuresGenerated < to_integer(unsigned(numberOfExposures)) then
							stateLeds <= "001";
							exposuresCalculatorSet <= '0';
							exposuresGenerated <= exposuresGenerated + 1;
							exposureStartRamWriteAddress 	<= std_logic_vector(to_unsigned(exposuresGenerated, exposuresRamAddressSize));
							exposureStartRamDataWrite		<= exposuresCalculatorCounter;
							
							exposuresSecondCount <= 1;
							
							generateExposuresNextState <= countSeconds;
						else
							generateExposuresNextState <= stop;
						end if;
						
					when countSeconds =>
						stateLeds <= "010";
						enableCounter <= '1';
						if exposuresSecondCount < to_integer(unsigned(intervalBetweenExposures)) then
							exposuresSecondCount <= exposuresSecondCount + 1;	
							generateExposuresNextState <= countSeconds;
						else
							generateExposuresNextState <= checkNumber;
						end if;
						
					when stop =>
						stateLeds <= "011";
						enableCounter <= '0';
						exposureGeneratorStopSignal <= '1';
						exposureStartRamWriteEnable <= '0';
						generateExposuresNextState <= idle;
						
					when others =>
						generateExposuresNextState <= idle;
					
				end case;
			end if;
		end process;
		
		generateExposureTimes_sequential: process (resetExposureGenerator, clk)
		begin
			if resetExposureGenerator = '1' then
				generateExposuresState <= idle;
			elsif rising_edge(clk) then
				generateExposuresState <= generateExposuresNextState;
			end if;
		end process;
		
		-- trigger camera
		process (selectedClock, resetTriggerProcess)
		begin
			if resetTriggerProcess = '1' then
				triggerSignal <= '1';
				lastExposureStartPerformed <= exposuresStartingTime;
				lastExposureEndPerformed <= exposuresStartingTime;
			elsif selectedClock = '1' then
				-- use futureClock (which is delayed with 0.5 sec) to round to nearest second by truncation instead of other complicated function
				
				-- START exposure
				if outputFutureCounterTrimmed = nextExposureStart (4 * syncTimeSize - 1 downto 0) then
					triggerSignal <= '0';
					lastExposureStartPerformed <= outputFutureCounterTrimmed;
					numberOfExposuresSoFar <= std_logic_vector (unsigned (numberOfExposuresSoFar) + 1);
--				end if;
--				
--				-- END exposure
--				if outputFutureCounterTrimmed = nextExposureEnd (4 * syncTimeSize - 1 downto 0) then
--					triggerSignal <= '1';
--					lastExposureEndPerformed <= outputFutureCounterTrimmed;
--					numberOfExposuresSoFar <= std_logic_vector (unsigned (numberOfExposuresSoFar) + 1);
--				end if;
				else
					triggerSignal <= '1';
					lastExposureEndPerformed <= outputFutureCounterTrimmed;
					
				end if;
				
			end if;
		end process;
		
		-- save exposure values to memory
--		process (clk)
--		variable addressVar: std_logic_vector (exposuresRamAddressSize - 1 downto 0);
--		begin
--			if (rising_edge(clk)) then
--	
--				-- enable memories - maybe not so good here!!!
--				exposureStartRamChipSelect <= '1';
--				exposureEndRamChipSelect <= '1';
--				
--				-- enable writes
--				exposureStartRamWriteEnable <= '1';
--				exposureEndRamWriteEnable <= '1';
--				
--				-- increment write address every clock
--				addressVar := std_logic_vector (unsigned (addressVar) + 1);
--				exposureStartRamWriteAddress <= addressVar;
--				exposureEndRamWriteAddress <= addressVar;
--				
--				-- get data to write at current address
--				exposureStartRamDataWrite <= exposureStarts (to_integer(unsigned(addressVar)));
--				exposureEndRamDataWrite <= exposureEnds (to_integer(unsigned(addressVar)));
--				
--			end if;
--		end process;
		
		-- load gpsTime from memory
		process (clk)
		variable addressVar: std_logic_vector (gpsRamAddressSize - 1 downto 0);
		begin
			if resetLoadGpsTime = '1' then
				loadGpsTimeState <= idle;
			elsif rising_edge(clk) then
				case loadGpsTimeState is
					when idle =>

						gpsDataValid <= '0';
						if quarterSecondSignal = '1' then
							gpsRamReadAddressSecondary <= "0000110";	-- [6]
							loadGpsTimeState <= firstCheck;
						else
							loadGpsTimeState <= idle; --next!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						end if;
						
					when firstCheck =>

						if gpsRamDataReadSecondary /= x"2C" then	-- [6] != "," 
							gpsRamReadAddressSecondary <= "0010000";	-- [16]
							loadGpsTimeState <= secondCheck;
						else
							loadGpsTimeState <= invalid;
						end if;
						
					when secondCheck =>

						if gpsRamDataReadSecondary = x"41" then -- [16] == "A" 
							gpsRamReadAddressSecondary <= "0000110"; --[6]
							charactersRead <= 0;
							loadGpsTimeState <= readTime;
						else
							loadGpsTimeState <= invalid;
						end if;
						
					when readTime =>

						gpsTime ((6 - charactersRead) * 4 - 1 downto (5 - charactersRead) * 4) <= gpsRamDataReadSecondary (3 downto 0);
						
						-- if gpsRamDataReadSecondary --optional verify valid time - digits ONLY or even '2' '3' '5' '9' '5' '9' max 
						if charactersRead < 5 then--gpsTimeSize  then -- if we haven't reached the 6th charcater in sequence (e.g. [11])
							gpsRamReadAddressSecondary <=  std_logic_vector(to_unsigned((6 + charactersRead + 1), gpsRamAddressSize));
							charactersRead <= charactersRead + 1; --poate nu ii ok
							loadGpsTimeState <= readTime;
						else
							charactersRead <= 0;
							loadGpsTimeState <= valid;
						end if;
							
					when invalid =>

						gpsDataValid <= '0';
						loadGpsTimeState <= idle;
						
					when valid =>

						gpsDataValid <= '1';
						loadGpsTimeState <= idle;
					
				end case;
			
			end if;
		end process;
		
		-- reset counter values (to correct small errors from FPGA clock and reset to a default value when needed)
		process (resetCounters, gpsDataValid)
		begin
			if resetCounters = '1' then
				futureCounterResetValue	<= defaultCounterResetValue & x"500";
				counterResetValue			<= defaultCounterResetValue & x"000";
			elsif gpsDataValid = '1' then
				futureCounterResetValue <= gpsTime & outputFutureCounter (11 downto 0);
				counterResetValue			<= gpsTime & outputCounter (11 downto 0);
			else
				futureCounterResetValue <= outputFutureCounterTrimmed & x"500";
				counterResetValue 		<= outputFutureCounterTrimmed & x"000";
			end if;
		end process;
		
		-- reset counters using set signal
		process (resetCounters, pulsePerSecondSignal, gpsDataValid)
		begin
			if resetCounters = '1' or pulsePerSecondSignal = '1' or gpsDataValid = '1' then --ar trebui pulsePerSecond sa fie chiar puls, ca sa stim timpu adevarat!!!!
				counterSet 			<= '1';
				futureCounterSet	<= '1';
			else
				counterSet 			<= '0';
				futureCounterSet	<= '0';
			end if;
		end process;
		
		
		process (outputCounter, clk)
		begin
			if rising_edge(clk) then
				if outputCounter (11 downto 0) = x"250" then -- nu ii buna conditia asta!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					quarterSecondSignal <= '1';
				else
					quarterSecondSignal <= '0';
				end if;
			end if;
		end process;
		
		-- generate exposures from startingTime, intervalBetween, exposureTime and numberOfExposures
		
--		
--		
--		
		-- MASTER reset
		process (masterReset)
		begin
			if masterReset = '1' then
				resetTriggerProcess 		<= '1';
				gpsReset 					<= '1';
				resetCounters				<= '1';
				resetLoadGpsTime			<= '1';
				resetExposureGenerator	<= '1';
				resetNextExposure			<= '1';
				resetRamDisplay			<= '1';
			else
				resetTriggerProcess 		<= '0';
				gpsReset						<= '0';
				resetCounters				<= '0';
				resetLoadGpsTime			<= '0';
				resetExposureGenerator	<= '0';
				resetNextExposure			<= '0';
				resetRamDisplay			<= '0';
			end if;
		end process;

		-- D registers for numberOfExposures and intervalBetweenExposures
		process (clk, setIntervals)
		begin
			if rising_edge(clk) then
				if setIntervals = '1' then
					intervalBetweenExposures <= sw (5 downto 0);
					numberOfExposures <= sw (15 downto 6);
				end if;
			end if;
		end process;
				
		--------------------------------------------------- concurrent instructions ------------------------------------------------
		
		exposureStartRamChipSelect <= '1';
		
		ramWriteCountClk <= gpsReceiveAcknowledge;
		
		ramReadCountClk 				<= step(0);
		masterReset 					<= step(1);
		startGenerator			 		<= step(2);
		--pulsePerSecondSignal			<= pulsePerSecond;
		switchSsdSignal				<= step(3);
		testGenerator					<= step(4);
		
		exposuresStartingTime		<= sw (15 downto 0) & x"00";
		setIntervals					<= step(4);
--		intervalBetweenExposures 	<= sw (5 downto 0);
--		exposureTimeInSeconds 		<= sw (3 downto 0);
--		numberOfExposures				<= sw (15 downto 6); 
							
		outputFutureCounterTrimmed <= outputFutureCounter (4 * outputSizeCounter - 1 downto 4 * (outputSizeCounter - syncTimeSize));
		
		selectedClock 					<= pulsePerSecondSignal;
		
		led 								<= std_logic_vector(to_unsigned(EXPOSURESTATES'POS(generateExposuresState), 3)) & exposuresCalculatorSet & exposureStateLeds & "0" & std_logic_vector(to_unsigned(exposuresGenerated, 4)) & "00" & gpsDataValid & triggerSignal;		--triggerSignal & "00" & quarterSecondSignal
		
		gpsTx 							<= rx;
		tx 								<= gpsRx;
		--tx									<= rx;	--test
		
		trigger 							<= triggerSignal;
		
end Behavioral;

