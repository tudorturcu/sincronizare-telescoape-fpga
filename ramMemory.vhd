----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:13:48 10/22/2014 
-- Design Name: 
-- Module Name:    ramMemory - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ramMemory is
	generic
	(
		ramAddressSize: natural;	-- |log_2(ramSize)|
		ramDataSize: natural
	);
	port 
	(
		readAddress				: in std_logic_vector (ramAddressSize - 1 downto 0);
		readAddressSecondary	: in std_logic_vector (ramAddressSize - 1 downto 0);
		writeAddress			: in std_logic_vector (ramAddressSize - 1 downto 0);
		clk						: in std_logic;
		chipSelect				: in std_logic;
		writeEnable				: in std_logic;
		dataIn					: in std_logic_vector (ramDataSize - 1 downto 0);
		dataOut					: out std_logic_vector (ramDataSize - 1 downto 0);
		dataOutSecondary		: out std_logic_vector (ramDataSize - 1 downto 0)
	);
	
end ramMemory;

architecture Behavioral of ramMemory is

type memoryType is array (0 to 2**ramAddressSize - 1) of std_logic_vector (ramDataSize - 1 downto 0);

signal memoryContent: memoryType :=		-- test initial data
	(
		others => (others => '0')
	);

begin

	process (clk, chipSelect, writeEnable)
	begin
		if (rising_edge(clk) and chipSelect = '1') then
			if (writeEnable = '1') then
				memoryContent(to_integer(unsigned(writeAddress))) <= dataIn;
			end if;
		end if;
	end process;

	dataOut 				<= memoryContent(to_integer(unsigned(readAddress)));
	dataOutSecondary 	<= memoryContent(to_integer(unsigned(readAddressSecondary)));

end Behavioral;

