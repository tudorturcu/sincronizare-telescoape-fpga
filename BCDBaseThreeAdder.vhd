----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:58:45 03/18/2015 
-- Design Name: 
-- Module Name:    BCDBaseThreeAdderWithTwist - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BCDBaseThreeAdderWithTwist is
	Port	(
				a			: in std_logic_vector (3 downto 0);
				b			: in std_logic_vector (3 downto 0);
				carryIn	: in std_logic;
				sum		: out std_logic_vector (3 downto 0);
				carryOut	: out std_logic
			);
end BCDBaseThreeAdderWithTwist;

architecture Behavioral of BCDBaseThreeAdderWithTwist is

begin

process(a, b, carryIn)
variable sum_temp : unsigned(4 downto 0);
begin
	 sum_temp := unsigned('0' & a) + unsigned('0' & b) + ("0000" & carryIn); 
	 if(sum_temp > 2) then
		  carryOut <= '1';
		  sum <= std_logic_vector(resize((sum_temp + "01010"),4));
	 else
		  carryOut <= '0';
		  sum <= std_logic_vector(sum_temp(3 downto 0));
	 end if; 

end process;   

end Behavioral;

