----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:48:42 11/30/2014 
-- Design Name: 
-- Module Name:    mathematicalSolutionToLossOfGpsSignal - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mathematicalSolutionToLossOfGpsSignal is
end mathematicalSolutionToLossOfGpsSignal;

architecture Behavioral of mathematicalSolutionToLossOfGpsSignal is

--signal exposuresStartingTimeInSeconds		: natural;
--signal outputFutureCounterInSeconds			: natural;
--signal timeSinceExposuresStartingTime		: natural;
--signal nrOfExposuresSoFar						: natural;
--signal timeUntilNextExposureStartInSeconds: natural;
--signal nextExposureStartInSeconds			: natural;
--signal nextExposureStartHours					: natural;
--signal nextExposureStartMinutes				: natural;
--signal nextExposureStartSeconds				: natural;
--signal nextExposureEndInSeconds				: natural;
--signal nextExposureEndHours					: natural;
--signal nextExposureEndMinutes					: natural;
--signal nextExposureEndSeconds					: natural;

begin

-- find next exposure start and next exposure end
--		process (selectedClock)
--		begin
--			if rising_edge(selectedClock) then
--				exposuresStartingTimeInSeconds <=	to_integer (unsigned (exposuresStartingTime (4 * (syncTimeSize - 0) - 1 downto 4 * (syncTimeSize - 1)))) * 10 * 3600 +
--																to_integer (unsigned (exposuresStartingTime (4 * (syncTimeSize - 1) - 1 downto 4 * (syncTimeSize - 2)))) * 1 * 3600 +
--																to_integer (unsigned (exposuresStartingTime (4 * (syncTimeSize - 2) - 1 downto 4 * (syncTimeSize - 3)))) * 10 * 60 +
--																to_integer (unsigned (exposuresStartingTime (4 * (syncTimeSize - 3) - 1 downto 4 * (syncTimeSize - 4)))) * 1 * 60 +
--																to_integer (unsigned (exposuresStartingTime (4 * (syncTimeSize - 4) - 1 downto 4 * (syncTimeSize - 5)))) * 10 * 1 +
--																to_integer (unsigned (exposuresStartingTime (4 * (syncTimeSize - 5) - 1 downto 4 * (syncTimeSize - 6)))) * 1 * 1;
--				
--				outputFutureCounterInSeconds <=		to_integer (unsigned (outputFutureCounter (4 * (outputSizeCounter - 0) - 1 downto 4 * (outputSizeCounter - 1)))) * 10 * 3600 +
--																to_integer (unsigned (outputFutureCounter (4 * (outputSizeCounter - 1) - 1 downto 4 * (outputSizeCounter - 2)))) * 1 * 3600 +
--																to_integer (unsigned (outputFutureCounter (4 * (outputSizeCounter - 2) - 1 downto 4 * (outputSizeCounter - 3)))) * 10 * 60 +
--																to_integer (unsigned (outputFutureCounter (4 * (outputSizeCounter - 3) - 1 downto 4 * (outputSizeCounter - 4)))) * 1 * 60 +
--																to_integer (unsigned (outputFutureCounter (4 * (outputSizeCounter - 4) - 1 downto 4 * (outputSizeCounter - 5)))) * 10 * 1 +
--																to_integer (unsigned (outputFutureCounter (4 * (outputSizeCounter - 5) - 1 downto 4 * (outputSizeCounter - 6)))) * 1 * 1;
--				
--				timeSinceExposuresStartingTime		<= outputFutureCounterInSeconds - exposuresStartingTimeInSeconds;
--				nrOfExposuresSoFar						<= timeSinceExposuresStartingTime / to_integer (unsigned (intervalBetweenExposures));
--				timeUntilNextExposureStartInSeconds	<= nrOfExposuresSoFar * to_integer (unsigned (intervalBetweenExposures));
--				
--				nextExposureStartInSeconds				<= outputFutureCounterInSeconds + timeUntilNextExposureStartInSeconds;
--				if nextExposureStartInSeconds >= 86400 then
--					nextExposureStartInSeconds <= nextExposureStartInSeconds - 86400;
--				end if;
--				nextExposureStartHours <= nextExposureStartInSeconds / 3600;
--				nextExposureStart (4 * (syncTimeSize - 0)- 1 downto 4 * (syncTimeSize - 1)) <= std_logic_vector (to_unsigned (nextExposureStartHours / 10, 4));			
--				nextExposureStart (4 * (syncTimeSize - 1)- 1 downto 4 * (syncTimeSize - 2)) <= std_logic_vector (to_unsigned (nextExposureStartHours mod 10, 4));
--				nextExposureStartMinutes <= nextExposureStartInSeconds mod 3600;
--				nextExposureStart (4 * (syncTimeSize - 2)- 1 downto 4 * (syncTimeSize - 3)) <= std_logic_vector (to_unsigned (nextExposureStartMinutes / 10, 4));
--				nextExposureStart (4 * (syncTimeSize - 3)- 1 downto 4 * (syncTimeSize - 4)) <= std_logic_vector (to_unsigned (nextExposureStartMinutes mod 10, 4));
--				nextExposureStartSeconds <= nextExposureStartInSeconds mod 60;
--				nextExposureStart (4 * (syncTimeSize - 4)- 1 downto 4 * (syncTimeSize - 5)) <= std_logic_vector (to_unsigned (nextExposureStartSeconds / 10, 4));
--				nextExposureStart (4 * (syncTimeSize - 5)- 1 downto 4 * (syncTimeSize - 6)) <= std_logic_vector (to_unsigned (nextExposureStartSeconds mod 10, 4));
--				
--				nextExposureEndInSeconds				<= nextExposureStartInSeconds + to_integer (unsigned(exposureTimeInSeconds));
--				if nextExposureEndInSeconds >= 86400 then
--					nextExposureEndInSeconds <= nextExposureEndInSeconds - 86400;
--				end if;
--				nextExposureEndHours <= nextExposureEndInSeconds / 3600;
--				nextExposureEnd (4 * (syncTimeSize - 0)- 1 downto 4 * (syncTimeSize - 1)) <= std_logic_vector (to_unsigned (nextExposureEndHours / 10, 4));			
--				nextExposureEnd (4 * (syncTimeSize - 1)- 1 downto 4 * (syncTimeSize - 2)) <= std_logic_vector (to_unsigned (nextExposureEndHours mod 10, 4));
--				nextExposureEndMinutes <= nextExposureEndInSeconds mod 3600;
--				nextExposureEnd (4 * (syncTimeSize - 2)- 1 downto 4 * (syncTimeSize - 3)) <= std_logic_vector (to_unsigned (nextExposureEndMinutes / 10, 4));
--				nextExposureEnd (4 * (syncTimeSize - 3)- 1 downto 4 * (syncTimeSize - 4)) <= std_logic_vector (to_unsigned (nextExposureEndMinutes mod 10, 4));
--				nextExposureEndSeconds <= nextExposureEndInSeconds mod 60;
--				nextExposureEnd (4 * (syncTimeSize - 4)- 1 downto 4 * (syncTimeSize - 5)) <= std_logic_vector (to_unsigned (nextExposureEndSeconds / 10, 4));
--				nextExposureEnd (4 * (syncTimeSize - 5)- 1 downto 4 * (syncTimeSize - 6)) <= std_logic_vector (to_unsigned (nextExposureEndSeconds mod 10, 4));
--				
--			end if;
--		end process;

end Behavioral;

