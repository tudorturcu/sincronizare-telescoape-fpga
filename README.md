VHDL project which uses a GPS signal to provide high timing accuracy for
synchronizing the triggering of 2 independent cameras situated in different 
locations.

Features:
    - gps data validation
    - timing synchronization when losing gps signal
    - generating a set of exposure times from a starting time and an interval
    between them
    - storing the last exposure performed and returning to the same time
    in the case of reseting the FPGA board


Application:
Each camera is mounted on a telescope at a different location which provides 
observations for the same satellite. 
The precise timing is important since the satellite moves relative to the stars
in the sky. By knowing the precise time for each of the camera, we can 
determine the distance to that satellite by using some sort of parallax.