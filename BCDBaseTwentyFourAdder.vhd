----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:38:09 03/18/2015 
-- Design Name: 
-- Module Name:    BCDBaseTwentyFourAdder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BCDBaseTwentyFourAdder is
	Port	(
				a			: in std_logic_vector (7 downto 0);
				b			: in std_logic_vector (7 downto 0);
				carryIn	: in std_logic;
				sum		: out std_logic_vector (7 downto 0);
				carryOut	: out std_logic
			);
end BCDBaseTwentyFourAdder;

architecture Behavioral of BCDBaseTwentyFourAdder is

begin

process(a, b, carryIn)
variable sum_units 	: unsigned (5 downto 0);
variable sum_tens		: unsigned (4 downto 0);
variable sum_temp 	: integer;
begin
	 sum_tens := unsigned( a(7 downto 4)) + unsigned( b(7 downto 4));
	 sum_units := unsigned('0' & a(3 downto 0)) + unsigned('0' & b(3 downto 0)) + ("0000" & carryIn);
	 sum_temp :=  to_integer(sum_tens) * 10 + to_integer(sum_units); 
	 
	 if(sum_temp < 24) then
		  carryOut <= '0';
		  
		  if (sum_units
		  --sum <= std_logic_vector(sum_temp(3 downto 0));
		  --sum <= std_logic_vector(resize((sum_temp + "01010"),4));
	 else
		  carryOut <= '1';
		  --sum <= std_logic_vector(sum_temp(3 downto 0));
	 end if; 

end process;   

end Behavioral;

