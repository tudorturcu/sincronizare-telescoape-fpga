----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:36:31 03/18/2015 
-- Design Name: 
-- Module Name:    BCDBaseTenAdder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BCDBaseTenAdder is
	Port	(
				a			: in std_logic_vector (3 downto 0);
				b			: in std_logic_vector (3 downto 0);
				carryIn	: in std_logic;
				sum		: out std_logic_vector (3 downto 0);
				carryOut	: out std_logic
			);

end BCDBaseTenAdder;

architecture Behavioral of BCDBaseTenAdder is

begin

process(a, b, carryIn)
variable sum_temp : unsigned(4 downto 0);
begin
	 sum_temp := unsigned('0' & a) + unsigned('0' & b) + ("0000" & carryIn); 
	 if(sum_temp > 9) then
		  carryOut <= '1';
		  sum <= std_logic_vector(resize((sum_temp + "00110"),4));
	 else
		  carryOut <= '0';
		  sum <= std_logic_vector(sum_temp(3 downto 0));
	 end if; 

end process;   

end Behavioral;

