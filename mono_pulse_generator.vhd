----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:23:55 02/22/2012 
-- Design Name: 
-- Module Name:    mono_pulse_generator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mono_pulse_generator is
Generic 	(
				pulseNumber: natural
			);

Port (
			clk: in std_logic;
			step: out std_logic_vector (pulseNumber - 1 downto 0);
			btn: in std_logic_vector (pulseNumber - 1 downto 0)
		);	
			
end mono_pulse_generator;

architecture Behavioral of mono_pulse_generator is

Signal cnt: std_logic_vector (15 downto 0);
Signal q1: std_logic_vector (pulseNumber - 1 downto 0);
Signal q2: std_logic_vector (pulseNumber - 1 downto 0);


begin

counter: process (clk)
begin
	if (rising_edge (clk)) then
			cnt <= cnt + 1;
	end if;		
end process counter;

d1: process (clk, cnt, btn)
begin
	if (rising_edge (clk))  then
		if (cnt = x"FFFF")	
			then q1 <= btn;
		end if;	
	end if;		
end process d1;

d2: process (clk, q1)
begin
	if (rising_edge (clk)) then
			q2 <= q1;
	end if;		
end process d2;

step <= (NOT q2) AND q1;

end Behavioral;

