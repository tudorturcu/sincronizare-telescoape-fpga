----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:36:15 02/29/2012 
-- Design Name: 
-- Module Name:    7SDD - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SDD is
    Port ( clk : in  STD_LOGIC;
           d1 : in  STD_LOGIC_VECTOR (3 downto 0);
           d2 : in  STD_LOGIC_VECTOR (3 downto 0);
           d3 : in  STD_LOGIC_VECTOR (3 downto 0);
           d4 : in  STD_LOGIC_VECTOR (3 downto 0);
           an : out  STD_LOGIC_VECTOR (3 downto 0);
           cat : out  STD_LOGIC_VECTOR (6 downto 0);
			  dp : out STD_LOGIC);
end SDD;

architecture Behavioral of SDD is

Signal count: std_logic_vector (15 downto 0);
Signal sel: std_logic_vector (1 downto 0);
Signal d: std_logic_vector (3 downto 0);

begin

counter:process (clk)			-- counter
begin
	if (rising_edge (clk)) then
		 count <= count + 1;
	end if;		
end process;

sel <= count (15 downto 14);

muxuri:process (sel, d1, d2, d3, d4) -- muxuri
begin
	if (sel = "00") then 
		an <= "1110"; 
		d <= d1;
	
	elsif (sel = "01") then 
		an <= "1101"; 
		d <= d2;
	
	elsif (sel = "10") then 
		an <= "1011"; 
		d <= d3;
	
	elsif (sel = "11") then 
		an <= "0111"; 
		d <= d4;
	else
		an <= "1111";
		d <= "0000";
	end if;
end process;

		--HEX-to-seven-segment decoder
--   d:   in    STD_LOGIC_VECTOR (3 downto 0);
--   cat:   out   STD_LOGIC_VECTOR (6 downto 0);
-- 
-- segment encoinputg
--      0
--     ---  
--  5 |   | 1
--     ---   <- 6
--  4 |   | 2
--     ---
--      3
   WITH d SELECT
   cat<= "1111001" when "0001",   --1
         "0100100" when "0010",   --2
         "0110000" when "0011",   --3
         "0011001" when "0100",   --4
         "0010010" when "0101",   --5
         "0000010" when "0110",   --6
         "1111000" when "0111",   --7
         "0000000" when "1000",   --8
         "0010000" when "1001",   --9
         "0001000" when "1010",   --A
         "0000011" when "1011",   --b
         "1000110" when "1100",   --C
         "0100001" when "1101",   --d
         "0000110" when "1110",   --E
         "0001110" when "1111",   --F
         "1000000" when others;   --0
	
	dp <= '0';
	
end Behavioral;

