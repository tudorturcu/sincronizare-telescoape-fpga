----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:07:08 10/21/2014 
-- Design Name: 
-- Module Name:    oneSecondClock - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity oneSecondClock is

	Generic 	
	(
		clockConstant: natural := 50000000;
		maximumVectorSize: natural := 32
	);
	
	Port 
	(
		clockIn: in std_logic;
		clockOut: out std_logic
	);

end oneSecondClock;

architecture Behavioral of oneSecondClock is

Signal count: std_logic_vector (maximumVectorSize - 1 downto 0);
Signal q: std_logic;

begin

process (clockIn)
begin

	if (rising_edge(clockIn)) then
		if (to_integer(unsigned(count)) = clockConstant / 2 - 1) then
			count <= std_logic_vector (to_unsigned(0, maximumVectorSize));
			q <= not q;
		else
			count <= std_logic_vector( unsigned(count) + 1);
		end if;
	end if;
end process;

clockOut <= q;

end Behavioral;

