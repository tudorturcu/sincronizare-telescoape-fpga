----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:30:38 01/18/2015 
-- Design Name: 
-- Module Name:    SSD_8digits - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SSD_8digits is
		Port ( clk 			: in  STD_LOGIC;
				  d1 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d2 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d3 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d4 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d5 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d6 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d7 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  d8 			: in  STD_LOGIC_VECTOR (3 downto 0);
				  dpVector 	: in 	STD_LOGIC_VECTOR (7 downto 0);
				  
				  an 			: out STD_LOGIC_VECTOR (7 downto 0);
				  cat 		: out STD_LOGIC_VECTOR (6 downto 0);
				  dp 			: out STD_LOGIC);
end SSD_8digits;

architecture Behavioral of SSD_8digits is

Signal count: std_logic_vector (15 downto 0);
Signal sel: std_logic_vector (2 downto 0);
Signal d: std_logic_vector (3 downto 0);

begin

counter : process (clk)			-- counter
begin
	if (rising_edge (clk)) then
		 count <= std_logic_vector( unsigned(count) + 1);
	end if;		
end process;

sel <= count (15 downto 13);


	WITH sel SELECT
		d <=
			d1 when "000",
			d2 when "001",
			d3 when "010",
			d4 when "011",
			d5 when "100",
			d6 when "101",
			d7 when "110",
			d8 when "111",
			x"0" when others;
		
	WITH sel SELECT
		an <= 
			"11111110" when "000",
			"11111101" when "001",
			"11111011" when "010",
			"11110111" when "011",
			"11101111" when "100",
			"11011111" when "101",
			"10111111" when "110",
			"01111111" when "111",
			"11111111" when others;

	
		--HEX-to-seven-segment decoder
--   d:   in    STD_LOGIC_VECTOR (3 downto 0);
--   cat:   out   STD_LOGIC_VECTOR (6 downto 0);
-- 
-- segment encoinputg
--      0
--     ---  
--  5 |   | 1
--     ---   <- 6
--  4 |   | 2
--     ---
--      3
   WITH d SELECT
		cat<= "1000000" when "0000",	 --0
				"1111001" when "0001",   --1
				"0100100" when "0010",   --2
				"0110000" when "0011",   --3
				"0011001" when "0100",   --4
				"0010010" when "0101",   --5
				"0000010" when "0110",   --6
				"1111000" when "0111",   --7
				"0000000" when "1000",   --8
				"0010000" when "1001",   --9
				"0001000" when "1010",   --A
				"0000011" when "1011",   --b
				"1000110" when "1100",   --C
				"0100001" when "1101",   --d
				"0000110" when "1110",   --E
				"0001110" when "1111",   --F
				"0111111" when others;   -- nil (-)
		
	dp <= dpVector (to_integer(unsigned(sel)));	
	
end Behavioral;

