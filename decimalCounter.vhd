----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:53:23 10/20/2014 
-- Design Name: 
-- Module Name:    decimalCounter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decimalCounter is
	Port 	(
				
				d: in std_logic_vector (3 downto 0);
				set: in std_logic;
				clk: in std_logic;
				
				q: out std_logic_vector (3 downto 0);
				overflow: out std_logic
			);
				
end decimalCounter;

architecture Behavioral of decimalCounter is

Signal count: std_logic_vector (3 downto 0);

begin

process (clk, set)
begin
	
	if (rising_edge(clk)) then
		
		if (set = '1') then
			count <= d;
		else
			if (count = "1001") then
				count <= "0000";
			else
				count <= std_logic_vector( unsigned(count) + 1);
			end if;	
		end if;
	
	end if;
	
	if (count = "1001") then
		overflow <= '1';
	else
		overflow <= '0';
	end if;
	
	q <= count;
	

end process;


end Behavioral;

